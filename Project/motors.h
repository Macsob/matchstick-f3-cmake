/*
 * Motors.h
 *
 *  Created on: 27 Oct 2020
 *      Author: maciek
 */

#pragma once

#include <inttypes.h>
#include "stm32f3xx_hal.h"

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;

class Motors {
private:
	volatile uint32_t * _comcapreg;			// capture/compare register
	TIM_HandleTypeDef * _timer;
	uint32_t _channel;
public:
	Motors(TIM_HandleTypeDef *timer, uint32_t channel, volatile uint32_t * comcapreg);
	void init();
	void setSpeed(uint16_t speed);		// 1000-2000us
};

extern Motors Motor_EDF;
