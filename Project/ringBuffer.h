/*
 * RingBuffer.h
 *
 *  Created on: 16 Oct 2020
 *      Author: maciek
 */
#pragma once

#include "stm32f3xx_hal.h"

#define UART_BUFFER_SIZE 512

class RingBuffer {
	UART_HandleTypeDef * _uart;
	unsigned char buffer[UART_BUFFER_SIZE];
	volatile unsigned int head;
	volatile unsigned int tail;
	void storeChar(unsigned char c);
	void put(char c);
public:
	RingBuffer(UART_HandleTypeDef* uart);
	void initRx();
	void sendString(char* s);
	int get();
	int IsDataAvailable();
	void Uart_isr_rx(UART_HandleTypeDef *huart); // put in IRQ handler
	void Uart_isr_tx(UART_HandleTypeDef *huart); // put in IRQ handler
};

extern RingBuffer Uart1_rx_buff;
extern UART_HandleTypeDef huart1;
