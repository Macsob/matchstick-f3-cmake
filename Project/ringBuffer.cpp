/*
 * RingBuffer.cpp
 *
 *  Created on: 16 Oct 2020
 *      Author: maciek
 */

#include "ringBuffer.h"
#include <stdio.h>
#include <string.h>

RingBuffer Uart1_rx_buff(&huart1);

RingBuffer::RingBuffer(UART_HandleTypeDef *uart) : buffer{0}, head(0), tail(0)
{
	// TODO Auto-generated constructor stub
	_uart = uart;
}

void RingBuffer::initRx()
{
	/* Enable the UART Data Register not empty Interrupt */
	__HAL_UART_ENABLE_IT(_uart, UART_IT_RXNE);
}

void RingBuffer::sendString(char *s)
{
	for (unsigned int i = 0; i < strlen(s); i++)
	{
		put(*(s + i));
	}
}

int RingBuffer::get(void)
{
	if (head == tail)
	{ // if the head isn't ahead of the tail, there are no characters
		return -1;
	}
	else
	{
		unsigned char c = buffer[tail];						// read from buffer
		tail = (unsigned int)(tail + 1) % UART_BUFFER_SIZE; // increment tail
		return c;
	}
}

void RingBuffer::put(char c)
{
	int i = (head + 1) % UART_BUFFER_SIZE;

	// keep more recent information
	buffer[head] = (char)c;
	head = i;

	__HAL_UART_ENABLE_IT(_uart, UART_IT_TXE); // Enable UART transmission interrupt
}

int RingBuffer::IsDataAvailable()
{
	return (uint16_t)(UART_BUFFER_SIZE + head - tail) % UART_BUFFER_SIZE;
}

void RingBuffer::storeChar(unsigned char c)
{
	unsigned int i = (unsigned int)(head + 1) % UART_BUFFER_SIZE;

	/* If the head would advance to the current location
	 * of the tail buffer overflows so don't
	 * write the character or advance the head. */
	if (i != tail)
	{
		buffer[head] = c;
		head = i;
	}
}

void RingBuffer::Uart_isr_rx(UART_HandleTypeDef *huart)
{
	uint32_t isrflags = READ_REG(huart->Instance->ISR);
	uint32_t cr1its = READ_REG(huart->Instance->CR1);

	/* if DR is not empty and the Rx Int is enabled */
	if (((isrflags & USART_ISR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
	{
		/******************

			 *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
			 *          error) and IDLE (Idle line detected) flags are cleared by software
			 *          sequence: a read operation to USART_SR register followed by a read
			 *          operation to USART_DR register.
			 * @note   RXNE flag can be also cleared by a read to the USART_DR register.
			 * @note   TC flag can be also cleared by software sequence: a read operation to
			 *          USART_SR register followed by a write operation to USART_DR register.
			 * @note   TXE flag is cleared only by a write to the USART_DR register.

		*********************/
		huart->Instance->ISR;					// Read status register
		unsigned char c = huart->Instance->RDR; // Read data register
		storeChar(c);							// store data in buffer
		return;
	}
}

void RingBuffer::Uart_isr_tx(UART_HandleTypeDef *huart)
{
	uint32_t isrflags = READ_REG(huart->Instance->ISR);
	uint32_t cr1its = READ_REG(huart->Instance->CR1);

	/*If interrupt is caused due to Transmit Data Register Empty */
	if (((isrflags & USART_ISR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET))
	{
		if (head == tail)
		{
			/* Buffer empty, so disable interrupts */
			__HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
		}

		else
		{
			/* There is more data in the output buffer. Send the next byte */
			unsigned char c = buffer[tail];
			tail = (tail + 1) % UART_BUFFER_SIZE;

			/******************
			*  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
			*          error) and IDLE (Idle line detected) flags are cleared by software
			*          sequence: a read operation to USART_SR register followed by a read
			*          operation to USART_DR register.
			* @note   RXNE flag can be also cleared by a read to the USART_DR register.
			* @note   TC flag can be also cleared by software sequence: a read operation to
			*          USART_SR register followed by a write operation to USART_DR register.
			* @note   TXE flag is cleared only by a write to the USART_DR register.

			*********************/

			huart->Instance->ISR;
			huart->Instance->RDR = c;
		}
		return;
	}
}
