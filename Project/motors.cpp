/*
 * Motors.cpp
 *
 *  Created on: 27 Oct 2020
 *      Author: maciek
 */

#include "motors.h"
#include <Eigen/Dense>

Motors Motor_EDF(&htim1, TIM_CHANNEL_4, &TIM1->CCR4);
// Motors ServoL(&htim1, TIM_CHANNEL_1, &TIM1->CCR1);

// Motors MotorL(&htim2, TIM_CHANNEL_2, &TIM2->CCR2);
// Motors MotorR(&htim2, TIM_CHANNEL_1, &TIM2->CCR1);

Motors::Motors(TIM_HandleTypeDef *timer, uint32_t channel, volatile uint32_t * comcapreg) {
	_timer = timer;
	_channel = channel;
	_comcapreg = comcapreg;
}

void Motors::init() {
	HAL_TIM_PWM_Start(_timer, _channel);				// start timer
}

/* set value 1000 - 2000 -> 1ms - 2ms (5% - 10% duty cycle) */
void Motors::setSpeed(uint16_t speed) {
	if (speed < 980) {
		speed = 980;
	} else if (speed > 2000) {
		speed = 2000;
	}
	*_comcapreg = speed;
}
